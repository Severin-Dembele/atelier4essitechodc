import './css/style.css';
import Header from './Header';
import Routing from '../src/Routes/Routing';
function App() {
  return (
    <div className="App" >
      <div className="Header">
        <Header />
      </div>
      <div className="contenair">
        <Routing />
       </div>
    </div>
  );
}
export default App;
