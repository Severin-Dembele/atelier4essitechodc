import './css/style.css';
import { Link } from "react-router-dom";

// npm i --save react-router-dom
export default function Header() {

    return (
        <>
            <ul>
                <li><a > <Link to="/"> Liste des Consommation </Link></a></li>
                <li><a > <Link to="/new"> Ajouter une consommation</Link> </a></li>
                <li><a ><Link to="/depense/{id}"> Mise a jour  une consommation </Link> </a></li>
                <li><a ><Link to="/test"> Test </Link> </a></li>
            </ul>
        </>
    )

}
