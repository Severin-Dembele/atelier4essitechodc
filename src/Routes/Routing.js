import React from 'react'
import {Routes} from "react-router-dom";
import {Route} from "react-router-dom";


import ListeDepenses from '../Components/ListeDepenses';
import NouvelleDepense from '../Components/NouvelleDepense';
import DepenseById from '../Components/DepenseById';
import NewComponent from '../Components/NewComponent';
export default function Routing() {

    return (

        <Routes>
            <Route path="/new" element={<NouvelleDepense />} />
            <Route path="/depense" element={<ListeDepenses />} />
            <Route path="/" element={<ListeDepenses />} />
            <Route path="depense/:id" element={<DepenseById/>} />
            <Route path="/test" element={<h1> Je suis le test</h1>} />
            <Route path="/autre" element={<h1> autre</h1>} />
            <Route path="/image/test" element={<NewComponent/>} />
            <Route path="*" element={<h1> Erreur 404</h1>} />
        </Routes>
    )

}
