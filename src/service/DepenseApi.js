import axios from 'axios';
const SERVER_API_BASE_URL = "http://localhost:8080/";


export function getAllDepenses(){
    return axios.get(SERVER_API_BASE_URL+'/getAll');
}
 export function  createDepense(data){
     return axios.post(SERVER_API_BASE_URL+'new',data);
}
export function   getDepenseById(id){
    return  axios.get(SERVER_API_BASE_URL +'get',id);
}
 export function  updatedepense(data,dataID){
    return axios.put(SERVER_API_BASE_URL + 'update/'+dataID,data);
}
 export function  deleteDepense(id){
    return axios.delete(SERVER_API_BASE_URL +'delete',id);
}
